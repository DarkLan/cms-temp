<?php
class Article extends Controller {
	public $articles = array();
	public $id;
	public $articleObj;

	public function __construct() {
		$this->articleObj = $this->model('Articles');
	}
	public function index() {
		$this->view('article/article', $this->articleObj->getArticles());
	}
	public function single($id = 1) {
		$this->view('article/article', $this->articleObj->getArticle($id));
	}

}