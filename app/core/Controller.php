<?php

abstract class Controller {

	public function indexPage() {
		$this->index();
	}

	protected function model ($modelName) {
		if(file_exists('../app/model/'.ucfirst($modelName).'.php')):
			require_once('../app/model/'.ucfirst($modelName).'.php');
		return new $modelName;
		endif;
	}

	public function view ($view, $results = array()) {
		require_once('../app/view/'.$view.'.php');
		if(file_exists('../app/view/'.$view.'.php')):
			require_once('../app/view/'.$view.'.php');
		else:
			echo 'no artirles';
		endif;
	}
}