<?php

class App {
	protected $controller = 'Home';
	protected $method = 'indexPage';
	protected $params = array();

	public function __construct() {
		$url = $this->getUrl();

		if(file_exists('../app/controller/'.$url[0].'.php')):
			$this->controller = $url[0];
			unset($url[0]);
		endif;

		require_once('../app/controller/'.$this->controller.'.php');

		$this->controller = new $this->controller;

		if(isset($url[1])) :
			if(method_exists($this->controller, $url[1])):
				$this->method = $url[1];
				unset($url[1]);
			endif;
		endif;
		$this->params = $url ? array_values($url) : array();
		/*echo '<pre>';
		echo print_r($this->params,2);
		echo '</pre><br>';*/

		call_user_func_array([$this->controller, $this->method], $this->params);
	}

	public function getUrl() {
		if(isset($_GET['url'])) :
			$url = explode('/', rtrim($_GET['url'], '/'));

			return $url;
		endif;
	}
}