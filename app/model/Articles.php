<?php

class Articles {
	protected $articles = [];

	public function getArticles () {
		$this->articles = [
			[
				'id' => 1,
				'title' => 'Article title 1',
				'prevText' => 'Some prev text',
				'user' => 'Alex',
				'date' => '10/16/2018'
			],
			[
				'id' => 20,
				'title' => 'Article title 2',
				'prevText' => 'Some prev text 222',
				'user' => 'Alex',
				'date' => '10/18/2018'
			],
			[
				'id' => 44,
				'title' => 'Article title 3',
				'prevText' => 'Some 333 text prev',
				'user' => 'Alex',
				'date' => '11/18/2018'
			]
		];

		return $this->articles;
	}

	public function getArticle ($id) {
		$this->articles = [
			[
				'id' => $id,
				'title' => 'Article title '.$id,
				'prevText' => 'Some prev text',
				'user' => 'Alex',
				'date' => '10/16/2018'
			]
		];
		return $this->articles;
	}
}